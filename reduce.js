function reduce(elements, cb, startingValue) {
    let initialValue = elements[0];

    if (startingValue === undefined) {
        initialValue = elements[0]
    } else {
        initialValue += startingValue
    }
    for (let index = 1; index < elements.length; index++) {
        initialValue = cb(initialValue, elements[index]);
    }
    return initialValue;
}

export default reduce;