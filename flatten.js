function flatten(elements) {
    let newArray = [];
    for (let value of elements) {
        if (Array.isArray(value) === true) {
            newArray.push(...flatten(value));
        } else {
            newArray.push(value);
        }
    }
    return newArray;
}

export default flatten;