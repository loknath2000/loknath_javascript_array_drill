function filter(elements, cb) {
    let newArray = [];
    for (let value of elements) {
        if (cb(value) % 2 === 0) {
            newArray.push(cb(value))
        }
    }
    return newArray;
}

export default filter;