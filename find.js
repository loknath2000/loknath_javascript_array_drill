function find(elements, cb) {
    for (let value of elements) {
        if (cb(value) === true) {
            return value;
        }
    }
    return "undefined";
}
export default find;