function map(elements, cb) {
    let newArray = [];
    for (let value of elements) {
        newArray.push(cb(value));
    }
    return newArray;
}

export default map;